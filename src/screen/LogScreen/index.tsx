import React, { useEffect, useState } from "react";
import axios from "axios";
// import { AgGridReact } from "ag-grid-react";

// import "ag-grid-community/styles/ag-grid.css";
// import "ag-grid-community/styles/ag-theme-alpine.css";

export default function LogScreen() {
  const [rowData, setRowData] = useState([]);

  const columnDefs = [
    { field: "Time" },
    { field: "Balance" },
    { field: "Posture" },
    { field: "Blink Interval" },
    { field: "Fixations" },
    { field: "Ambient Light" },
  ];

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('');
        setRowData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  console.log(rowData);

  return (
    <div>
      <div className="bg-white shadow-md p-4">
        <div className="container mx-auto">
          <div className="grid grid-cols-4 gap-4">
            <div className="col-span-1 flex items-center justify-center">
              <input
                type="text"
                placeholder="Search"
                className="bg-stone-100 shadow-md text-white rounded-md py-2 pl-4  focus:outline-none focus:ring focus:border-blue-300"
              />
              <button className="absolute right-3 top-1/2 transform -translate-y-1/2 text-white">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="h-5 w-5"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M21 21l-6-6m-2 0a8 8 0 1116 0 8 8 0 01-16 0z"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="ag-theme-alpine h-screen w-800">
        <AgGridReact rowData={arrayData} columnDefs={columnDefs}></AgGridReact>
      </div> */}
    </div>
  );
}
