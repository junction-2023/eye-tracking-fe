import { BsBell } from 'react-icons/bs'
import { IoIosArrowDown } from 'react-icons/io'

export default function ChangingStateScreen() {
    return (
        <div className='w-full flex flex-col p-8'>
            <div className='flex items-center justify-between'>
                <h3 className='text-3xl text-default'>Focus Measurement</h3>
                <div className='flex items-center'>
                    <div className='bg-white py-2 px-3 shadow-md rounded-md hover:shadow-lg transition-shadow cursor-pointer'>
                        <BsBell className='text-xl' />
                    </div>
                    <div className='flex justify-center  bg-white py-2 px-3 shadow-md rounded-md ml-4 relative hover:shadow-lg transition-shadow cursor-pointer'>
                        <img
                            className='w-6 h-6 rounded-full '
                            src='https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/415c73ae-5d9b-4a66-9155-589f0f5199a3/d9tvd1s-1c9b5d19-23ad-437a-bff4-9b16240baabf.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzQxNWM3M2FlLTVkOWItNGE2Ni05MTU1LTU4OWYwZjUxOTlhM1wvZDl0dmQxcy0xYzliNWQxOS0yM2FkLTQzN2EtYmZmNC05YjE2MjQwYmFhYmYuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.yNFFYgwZE6u-35TkCYxr0_fTkZ-IRN44DHWAuxIyVes'
                            alt='user avatar'
                        />
                        <h4 className='font-medium mx-3 text-default'>Tuong</h4>
                        <div className='flex justify-center items-center'>
                            <IoIosArrowDown className=''></IoIosArrowDown>
                        </div>
                    </div>
                </div>
            </div>
            <div></div>
        </div>
    )
}
