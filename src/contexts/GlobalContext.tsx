import React from 'react'

interface IGlobalContext {
    isOpen: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

const DEFAULT_CONTEXT: IGlobalContext = {
    isOpen: false,
    setOpen: () => {},
}

const GlobalContext = React.createContext<IGlobalContext>(DEFAULT_CONTEXT)

export const GlobalContextProvider: React.FC<{ children: any }> = ({ children }) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(DEFAULT_CONTEXT.isOpen)

    const setOpen = () => {
        setIsOpen((prev) => !prev)
    }

    const value: IGlobalContext = {
        isOpen,
        setOpen,
    }

    return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}

export default GlobalContext
