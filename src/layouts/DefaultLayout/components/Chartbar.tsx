import { TEST_DATA } from '~root/constants'
import { Line } from 'react-chartjs-2'

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js'

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend)

export const options = {
    responsive: true,
    plugins: {
        legend: {
            position: 'top' as const,
        },
        title: {
            display: true,
            text: 'Chart.js Line Chart',
        },
    },
}



const labels = TEST_DATA.map((_record, index) => index.toFixed(2))

export const data = {
    labels,
    datasets: [
        {
            label: 'Dataset 1',
            data: labels.map((_label, index) => TEST_DATA[index + 60]),
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
        },
    ],
}

export default function LineChart() {
    return <Line options={options} data={data} />
}