// import React from 'react';
// import { useState } from 'react';

import { BiHomeSmile } from 'react-icons/bi';
import { FiSettings } from 'react-icons/fi';
import { RiAccountCircleLine } from 'react-icons/ri';
import { BiFile } from 'react-icons/bi'; // Assuming TbFileDescription was a typo, using BiFile instead

export default function Sidebar() {
    // const [click, setClick] = useState(false);

    // const handleClick = () => {
    //     setClick(!click);
    // };

    const routes = [
        {
            label: 'Home',
            icon: <BiHomeSmile />,
            href: '/changing-state',
        },
        {
            label: 'Logs',
            icon: <BiFile />,
            href: '/log-state',
        },
        {
            label: 'Settings',
            icon: <FiSettings />,
            href: '/settings',
        },
        {
            label: 'My Account',
            icon: <RiAccountCircleLine />,
            href: '/my-account',
        },
    ];


    return (
        <div className='w-1/5 h-screen bg-slate-50'>
            <div className='p-5 pt-10'>
                <div className='flex p-2'>
                    <img
                        src='https://marketplace.canva.com/vhV-E/MAFHSMvhV-E/1/tl/canva-MAFHSMvhV-E.png'
                        alt=''
                        className='w-8 h-8 '
                    />
                    <p className='font-semibold text-default text-lg pl-2 pt-2'>Myfocus.io</p>
                </div>
                <div className='list-none pt-5'>
                    {routes.map((route) => (
                        <div className="font-medium text-default text-lg my-10 py-2.5 pl-3 rounded-md hover:bg-light-green hover:text-white">
                            {route.icon}
                            {route.label}
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

