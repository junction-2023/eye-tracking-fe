import React from 'react';
import Sidebar from './components/Sidebar';
import Chartbar from './components/Chartbar';

export default function DefaultLayout(props: { children: React.ReactNode }) {
    return (
        <div className='w-full h-screen flex items-center'>
            <Sidebar />
            <Chartbar />
            <div className='flex-1 h-screen bg-slate-40'>{props.children}</div>
        </div>
    )
}
